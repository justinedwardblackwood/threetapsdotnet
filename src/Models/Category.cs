﻿namespace Models
{
	public sealed class Category
	{
		public string group_name { get; set; }

		public string group_code { get; set; }

		public string code { get; set; }

		public string name { get; set; }
	}
}
