﻿using Newtonsoft.Json;

namespace Models
{
	public sealed class PostingLocation
	{
		public string city { get; set; }

		public string metro { get; set; }

		public string locality { get; set; }

		public string country { get; set; }

		public string region { get; set; }

		public string zipcode { get; set; }

		[JsonProperty("long")]
		public decimal longitude { get; set; }

		public string county { get; set; }

		public string state { get; set; }

		[JsonProperty("lat")]
		public decimal latitude { get; set; }

		public string formatted_address { get; set; }

		public long accuracy { get; set; }
	}
}
