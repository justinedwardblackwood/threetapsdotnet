﻿namespace Models.Request.Reference
{
	public sealed class LocationsRequest
	{
		public string level { get; set; }

		public string country { get; set; }

		public string state { get; set; }

		public string metro { get; set; }

		public string region { get; set; }

		public string county { get; set; }

		public string city { get; set; }

		public string locality { get; set; }

		public decimal? min_lat { get; set; }

		public decimal? min_long { get; set; }

		public decimal? max_lat { get; set; }

		public decimal? max_long { get; set; }
	}
}
