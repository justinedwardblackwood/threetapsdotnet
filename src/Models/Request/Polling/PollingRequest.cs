﻿namespace Models.Request.Polling
{
	public sealed class PollingRequest
	{
		public long? anchor { get; set; }

		public string source { get; set; }

		public string category_group { get; set; }

		public string category { get; set; }

		public string country { get; set; }

		public string state { get; set; }

		public string metro { get; set; }

		public string region { get; set; }

		public string county { get; set; }

		public string city { get; set; }

		public string locality { get; set; }

		public string zipcode { get; set; }

		public string status { get; set; }

		// List of fields, separated by commas, that should be returned to the caller.
		// Defaults returned are: id source, category, location, external_id, external_url, heading, timestamp.
		public string retvals { get; set; }
	}
}
