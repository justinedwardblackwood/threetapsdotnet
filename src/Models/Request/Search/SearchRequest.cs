﻿namespace Models.Request.Search
{
	public sealed class SearchRequest
	{
		public string category_group { get; set; }

		public string category { get; set; }

		public string country { get; set; }

		public string state { get; set; }

		public string metro { get; set; }

		public string region { get; set; }

		public string county { get; set; }

		public string city { get; set; }

		public string locality { get; set; }

		public string zipcode { get; set; }

		public string radius { get; set; }

		// TODO: Create an attribute for mapping from this name to "lat"
		public string latitude { get; set; }

		// TODO: Create an attribute for mapping from this name to "long"
		public string longitude { get; set; }

		public string source { get; set; }

		public string external_id { get; set; }

		public string heading { get; set; }

		public string body { get; set; }

		public string text { get; set; }

		public string timestamp { get; set; }

		public long id { get; set; }

		public string price { get; set; }

		public string currency { get; set; }

		public string annotations { get; set; }

		public string status { get; set; }

		public string has_image { get; set; }

		public string include_deleted { get; set; }

		public string only_deleted { get; set; }
	}
}
