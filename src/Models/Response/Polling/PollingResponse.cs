﻿using System.Collections.Generic;

namespace Models.Response.Polling
{
	public sealed class PollingResponse
	{
		public bool success { get; set; }

		public string error { get; set; }

		public long anchor { get; set; }

		public List<Posting> postings { get; set; }
	}
}
