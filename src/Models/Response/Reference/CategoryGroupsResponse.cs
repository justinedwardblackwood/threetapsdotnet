﻿using System.Collections.Generic;

namespace Models.Response.Reference
{
	public sealed class CategoryGroupsResponse
	{
		public bool success { get; set; }

		public string error { get; set; }

		public List<Source> category_groups { get; set; }
	}
}
