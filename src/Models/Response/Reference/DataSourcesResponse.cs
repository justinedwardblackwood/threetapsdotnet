﻿using System.Collections.Generic;

namespace Models.Response.Reference
{
	public sealed class DataSourcesResponse
	{
		public bool success { get; set; }

		public string error { get; set; }

		public List<Source> sources { get; set; }
	}
}
