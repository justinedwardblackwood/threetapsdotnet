﻿using System.Collections.Generic;

namespace Models.Response.Reference
{
	public sealed class CategoriesResponse
	{
		public bool success { get; set; }

		public string error { get; set; }

		public List<Category> categories { get; set; }
	}
}
