﻿using System.Collections.Generic;

namespace Models.Response.Reference
{
	public sealed class LocationsResponse
	{
		public bool success { get; set; }

		public string error { get; set; }

		public List<ReferenceLocation> locations { get; set; }
	}
}
