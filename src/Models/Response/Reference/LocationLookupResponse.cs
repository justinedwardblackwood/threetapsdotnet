﻿namespace Models.Response.Reference
{
	public sealed class LocationLookupResponse
	{
		public bool success { get; set; }

		public string error { get; set; }

		public ReferenceLocation location { get; set; }
	}
}
