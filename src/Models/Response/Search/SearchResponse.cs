﻿using System.Collections.Generic;

namespace Models.Response.Search
{
	public sealed class SearchResponse
	{
		public long next_page { get; set; }

		public decimal time_search { get; set; }

		public bool success { get; set; }

		public decimal time_taken { get; set; }

		public long anchor { get; set; }

		public decimal time_fetch { get; set; }

		public long num_matches { get; set; }

		public List<Posting> postings { get; set; }

		public long next_tier { get; set; }
	}
}
