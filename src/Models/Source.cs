﻿namespace Models
{
	public sealed class Source
	{
		public string code { get; set; }

		public string name { get; set; }
	}
}
