﻿using System.Collections.Generic;

namespace Models
{
	public sealed class Posting
	{
		public string source { get; set; }

		public string external_url { get; set; }

		public string status { get; set; }

		public string heading { get; set; }

		public string body { get; set; }

		public string currency { get; set; }

		public long timestamp { get; set; }

		public string category { get; set; }

		public string html { get; set; }

		public string category_group { get; set; }

		public long id { get; set; }

		public string immortal { get; set; }

		public List<List<Image>> images { get; set; }

		public PostingLocation location { get; set; }

		public string language { get; set; }

		public bool deleted { get; set; }

		public long account_id { get; set; }

		public List<Annotation> annotations { get; set; }

		public long external_id { get; set; }

		public decimal? price { get; set; }

		public long expires { get; set; }
	}
}
