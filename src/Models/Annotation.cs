﻿namespace Models
{
	public sealed class Annotation
	{
		public string source_continent { get; set; }

		public string status { get; set; }

		public string proxy_ip { get; set; }

		public string source_neighborhood { get; set; }

		public string source_cat { get; set; }

		public string phone { get; set; }

		public string source_state { get; set; }

		public string source_account { get; set; }

		public long original_posting_date { get; set; }

		public string source_loc { get; set; }

		public string category_group { get; set; }

		public string source_subcat { get; set; }

		public string category { get; set; }

		public string source_subloc { get; set; }
	}
}
