﻿namespace Models
{
	public sealed class ReferenceLocation
	{
		public string code { get; set; }

		public string short_name { get; set; }

		public string full_name { get; set; }

		public decimal bounds_min_lat { get; set; }

		public decimal bounds_min_long { get; set; }

		public decimal bounds_max_lat { get; set; }

		public decimal bounds_max_long { get; set; }
	}
}
