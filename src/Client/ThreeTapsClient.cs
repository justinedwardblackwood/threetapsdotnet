﻿using Models.Request.Polling;
using Models.Request.Reference;
using Models.Request.Search;
using Models.Response.Polling;
using Models.Response.Reference;
using Models.Response.Search;

namespace Client
{
	public class ThreeTapsClient
	{
		public ThreeTapsClient(string authToken)
		{
			m_authToken = authToken;
			m_searchEndpoint = Properties.Settings.Default.SearchEndpoint;
			m_pollingEndpoint = Properties.Settings.Default.PollingEndpoint;
			m_referenceEndpoint = Properties.Settings.Default.ReferenceEndpoint;
		}

		public ThreeTapsClient(string authToken, string searchEndpoint, string pollingEndpoint, string referenceEndpoint)
		{
			m_authToken = authToken;
			m_searchEndpoint = searchEndpoint;
			m_pollingEndpoint = pollingEndpoint;
			m_referenceEndpoint = referenceEndpoint;
		}

		public SearchResponse Search(SearchRequest searchRequest)
		{
			return RequestUtility.ExecuteGetRequest<SearchResponse, SearchRequest>(m_searchEndpoint, m_authToken, searchRequest);
		}

		public AnchorResponse GetAnchor(AnchorRequest anchorRequest)
		{
			string baseUrl = string.Concat(m_pollingEndpoint, "/anchor");

			return RequestUtility.ExecuteGetRequest<AnchorResponse, AnchorRequest>(baseUrl, m_authToken, anchorRequest);
		}

		public PollingResponse GetPoll(PollingRequest pollingRequest)
		{
			string baseUrl = string.Concat(m_pollingEndpoint, "/poll");

			return RequestUtility.ExecuteGetRequest<PollingResponse, PollingRequest>(baseUrl, m_authToken, pollingRequest);
		}

		public DataSourcesResponse GetDataSources(DataSourcesRequest dataSourcesRequest)
		{
			string baseUrl = string.Concat(m_referenceEndpoint, "/sources");

			return RequestUtility.ExecuteGetRequest<DataSourcesResponse, DataSourcesRequest>(baseUrl, m_authToken, dataSourcesRequest);
		}

		public CategoryGroupsResponse GetCategoryGroups(CategoryGroupsRequest categoryGroupsRequest)
		{
			string baseUrl = string.Concat(m_referenceEndpoint, "/category_groups");

			return RequestUtility.ExecuteGetRequest<CategoryGroupsResponse, CategoryGroupsRequest>(baseUrl, m_authToken, categoryGroupsRequest);
		}

		public CategoriesResponse GetCategories(CategoriesRequest categoriesRequest)
		{
			string baseUrl = string.Concat(m_referenceEndpoint, "/categories");

			return RequestUtility.ExecuteGetRequest<CategoriesResponse, CategoriesRequest>(baseUrl, m_authToken, categoriesRequest);
		}

		public LocationsResponse GetLocations(LocationsRequest locationsRequest)
		{
			string baseUrl = string.Concat(m_referenceEndpoint, "/locations");

			return RequestUtility.ExecuteGetRequest<LocationsResponse, LocationsRequest>(baseUrl, m_authToken, locationsRequest);
		}

		public LocationLookupResponse GetLocationLookup(LocationLookupRequest locationLookupRequest)
		{
			string baseUrl = string.Concat(m_referenceEndpoint, "/locations/lookup");

			return RequestUtility.ExecuteGetRequest<LocationLookupResponse, LocationLookupRequest>(baseUrl, m_authToken, locationLookupRequest);
		}

		private readonly string m_searchEndpoint;
		private readonly string m_pollingEndpoint;
		private readonly string m_referenceEndpoint;

		private readonly string m_authToken;
	}
}
