﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;

using Newtonsoft.Json;

namespace Client
{
	public static class RequestUtility
	{
		public static TResponse ExecuteGetRequest<TResponse, TRequest>(string baseUrl, string authToken, TRequest request)
		{
			Dictionary<string, string> parameters = request.ConvertRequestToParameters();

			string concatenatedParameters = string.Empty;
			
			if (parameters.Any())
			{
				concatenatedParameters = parameters
					.Select(s => string.Concat(s.Key, "=", s.Value))
					.Aggregate((current, item) => string.Concat(current, "&", item));
			}

			string url = string.Format("{0}/?auth_token={1}", baseUrl, authToken);

			if (concatenatedParameters.Length > 0)
				url = string.Concat(url, "&", concatenatedParameters);

			HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);

			webRequest.ContentType = "application/x-www-form-urlencoded";
			webRequest.Accept = "application/json";

			string jsonResponse;

			using (WebResponse webResponse = webRequest.GetResponse())
			using (Stream str = webResponse.GetResponseStream())
			{
				if (str == null)
					throw new InvalidOperationException("ResponseStream was null.");

				using (StreamReader sr = new StreamReader(str))
					jsonResponse = sr.ReadToEnd();
			}

			return JsonConvert.DeserializeObject<TResponse>(jsonResponse);
		}

		public static Dictionary<string, string> ConvertRequestToParameters<T>(this T request)
		{
			Dictionary<string, string> parameters = new Dictionary<string, string>();

			Type type = typeof(T);
			List<PropertyInfo> propertyInfos = type.GetProperties().ToList();

			propertyInfos.ForEach(f =>
				{
					string name = f.Name;
					object obj = f.GetValue(request, null);

					if (obj != null)
					{
						string value = Convert.ToString(obj);
						parameters.Add(name, value);
					}
				});

			return parameters;
		}
	}
}
